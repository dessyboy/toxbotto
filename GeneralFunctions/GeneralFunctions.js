const fs = require('fs');

class generalfunctions
{
	constructor(config)
	{
		this.config = config
		this.LoadConfigs()
	}

	LoadConfigs()
	{
		console.log("Loading Configs")

		for (var key in this.config)
		{
			this.LoadConfig(key, false)
		}
		
	}

	LoadConfig(target, output)
	{
		let config = null
		let rawdata = fs.readFileSync('./Confs/'+this.config[target].cofigfile);
		
		try {
			config = JSON.parse(rawdata);
			if(output){tfunct.send(target, "Config was updated successfully")}
			configs[target] = config

			if(timedmsgs.hasOwnProperty(target) == false){timedmsgs[target] = []}

			this.StartTimedMessages(target)
			this.DeleteOldTimedMessages(target)
		} catch (e) {
			if(output){tfunct.send(target, "An error occured during Config update. Please check the config file for syntax errors.")}
			console.error(target + ":");
			return console.error(e);
		}
	}


	StartTimedMessages(target)
	{
		// Stop old and delete timers
		if(timedmsgs.hasOwnProperty(target) == true)
		{
			for( var i = 0, length = timedmsgs[target].length; i < length; i++ )
			{
				clearInterval(timedmsgs[target][i]);
			}

		}

		// Start new timers
		var keys = Object.keys(configs[target].timedmessages);
	    for( var j = 0, length = keys.length; j < length; j++ )
	    {
	    	let msg = configs[target].timedmessages[keys[j]].msg
	    	let TimedMessage = setInterval(() => this.TimedMessages(target, msg), configs[target].timedmessages[keys[j]].minutes * 60000);
			timedmsgs[target].push(TimedMessage)
		}
	}

	DeleteOldTimedMessages(target)
	{
		// Select Timers to Delete
		for(var i = timedmsgs[target].length -1; i > -1; i--)
		{
			if(timedmsgs[target][i]._destroyed)
			{
				timedmsgs[target].splice(i, 1);
			}		
		}
	}


	async TimedMessages(target, msg)
	{
		var variable = {}
		var online = await tapi.getUptime(target)
		if(online != "0")
		{
			variable['room-id'] = await tapi.resolveUser(target.substring(1))

			// Compute Lists
			if(msg.includes('[') & msg.includes(']'))
			{
				msg = this.ParseListGetRandom(msg)
			}

			// Compute Variables
			if(msg.includes('{') & msg.includes('}'))
			{
				msg = this.ParseVariables(msg, variables, target)
			}			
			tfunct.send(target, await msg)			
		}
	}

	ResetCounters(target)
	{
		var keys = Object.keys(configs[target].counters);
	    for( var i = 0,length = keys.length; i < length; i++ )
	    {
	    	configs[target].counters[keys[i]] = 0;
		}
	}

	SubDuration(context)
	{
		if(context['badge-info'] == null){return "0";}
		if(context['badge-info'].subscriber != null){return context['badge-info'].subscriber + " months";}
		if(context['badge-info'].founder    != null){return context['badge-info'].founder    + " months";}
	}

	Help(target, prefix, commands)
	{	
		var keys = Object.keys(commands)
		var string = "Commands: "
		for( var i = 0,length = keys.length; i < length; i++ )
	    {
	    	string = string + prefix + keys[i] + " "
		}
		tfunct.send(target, string)
	}

	/// Parser:

	async Parser(input, counters, variables, target)
	{
		// Compute Lists
		if(input.includes('[') & input.includes(']'))
		{
			input = this.ParseListGetRandom(input)
		}

		// Compute Counters
		if(input.includes('{ctr:') & input.includes('}'))
		{
			input = this.ParseCounters(input, counters)
		}

		// Compute Variables
		if(input.includes('{') & input.includes('}') & !input.includes('{ctr:'))
		{
			input = this.ParseVariables(input, variables, target)
		}

		input = await input
		if(input.includes('(') & input.includes(')') & input.includes(':'))
		{
			 input = this.ParseIfClause(input)
		}
	    return await input
	}

	ParseList(input)
	{
		input = input.replace(/\[|\]/g, "")
		input = input.split(";")
		return input
	}

	ParseListGetRandom(input)
	{
		input = input.replace(/\[|\]/g, "")
		input = input.split(";")
		var index  = Math.floor(Math.random() * input.length)
		input = input[index]
		return input		
	}

	async ParseVariables(input, variables, target)
	{
		if(input.includes("{user}"))			{ input = input.replace(/{user}/g,				  variables.username)}
		if(input.includes("{broadcaster}"))		{ input = input.replace(/{broadcaster}/g,		  target.substring(1))}
		if(input.includes("{sub}"))				{ input = input.replace(/{sub}/g,				  this.SubDuration(variables))}
		if(input.includes("{followage}"))		{ input = input.replace(/{followage}/g,		await tapi.getFollowage	(target , variables['room-id'], variables['user-id']))}
		if(input.includes("{title}"))			{ input = input.replace(/{title}/g,			await tapi.getTitle		(target, variables['room-id']))}
		if(input.includes("{game}"))			{ input = input.replace(/{game}/g,			await tapi.getGame  	(target, variables['room-id']))}
		if(input.includes("{clip}"))			{ input = input.replace(/{clip}/g,			await tapi.clip			(target, variables['room-id']))}
		if(input.includes("{uptime}"))			{ input = input.replace(/{uptime}/g,		await tapi.getUptime	(target))}
		return input
	}

	ParseCounters(input, counters)
	{
		var ctr = input.replace(/.*{ctr:|:inc}.*|:show}.*/g, "")
	        
		if(input.includes('inc'))
		{
			counters[ctr] = counters[ctr] + 1
			input = input.replace(`{ctr:${ctr}:inc}`,"")
		}
			
		if(input.includes('show'))
		{
			input = input.replace(`{ctr:${ctr}:show}`,counters[ctr])
		}
		return input
	}

	ParseIfClause(input)
	{
		input = input.replace(/\(|\)/g, "")
		input = input.split(":")
		
		if(input[0].includes("=="))
		{
			var condition = input[0].split("==")
			if(condition[0] == condition[1])
			{
				return input[1]
			}
			else input
			{
				return input[2]
			}			
		}
		else if(input[0].includes("!="))
		{
			var condition = input[0].split("!=")
			if(condition[0] == condition[1])
			{
				return input[2]
			}
			else input
			{
				return input[1]
			}
		}
		else
		{
			return "IfClauseError"
		}
	}	

	subparser(input, username, channel, recipient, months)
	{
		if(input.includes("{user}"))		{ input = input.replace(/{user}/g,		username)}
		if(input.includes("{channel}"))		{input = input.replace(/{channel}/g,	channel)}
		if(input.includes("{recipient}"))	{input = input.replace(/{recipient}/g,	recipient)}
		if(input.includes("{months}"))      {input = input.replace(/{months}/g,		months)}
	    return input
	}

	///Parser End
}
module.exports.generalfunctions = generalfunctions;