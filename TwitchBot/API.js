const fetch = require('node-fetch');
var moment = require('moment');
const fs = require('fs');


class twitchapi
{
  client_id     = "";
  access_token  = "";
  refresh_token = "";

  constructor(config)
  {
    this.refreshOauth()
    var OauthRefresher = setInterval(() => this.refreshOauth(), 7200000);    
  }

  refreshOauth()
  {
    let rawdata = fs.readFileSync(credpath + "tapi.json");
    try {
      var creds = JSON.parse(rawdata);
      this.client_id = creds.client_id

      var formBody = [];
      var details = {
          "grant_type"   : "refresh_token",
          "client_id"    : creds.client_id,
          "client_secret": creds.client_secret,
          "refresh_token": creds.refresh_token   
      };

      //URL Encoding
      for (var property in details) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
      }
      formBody = formBody.join("&");


      fetch('https://id.twitch.tv/oauth2/token',
      {
        "method" :'POST',
        "headers": {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        "body": formBody
      })
      .then(resp => resp.json())
      .then(resp => this.setToken(resp))

    } catch (e) {
      return console.error(e);
    }
  }
  
  setToken(response)
  {
    this.access_token  = response.access_token
    this.refresh_token = response.refresh_token

    let rawdata = fs.readFileSync(credpath + "tapi.json");
    try {
      var creds = JSON.parse(rawdata);

      creds["refresh_token"] = this.refresh_token

      fs.writeFileSync(credpath + "tapi.json", JSON.stringify(creds), (err) => {if (err) throw err;})
      console.log("oauth2 token resfreshed")
    } catch (e) {
      return console.error(e);
    }
  }

  async getGame (target, room_id) {
    var response = null;
    const game = fetch(`https://api.twitch.tv/helix/channels?broadcaster_id=${room_id}`,
    {
      "headers": 
      {
        "Client-ID": this.client_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp => {return this.Game(target, resp.data[0]);});
    return await game
  }

  Game(target, resp)
  {
    if(resp != undefined)
    {
      return resp.game_name
    }
    else
    {
      return "'404 Game Not Found'"
    }
  }

  /*
  {
    data: [
      {
        broadcaster_id: '246753970',
        broadcaster_login: 'dessy_boy',
        broadcaster_name: 'dessy_boy',
        broadcaster_language: 'en',
        game_id: '500561',
        game_name: 'Coding for Carrots',
        title: 'Testing Stuff',
        delay: 0
      }
    ]
  }
  */

  async getTitle (target, room_id) {
    var response = null;
    const game = fetch(`https://api.twitch.tv/helix/channels?broadcaster_id=${room_id}`,
    {
      "headers": 
      {
        "Client-ID": this.client_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp => {return this.Title(target, resp.data[0]);});
    return await game
  }

  Title(target, resp)
  {
    if(resp != undefined)
    {
      return resp.title
    }
    else
    {
      return "'404 Title Not Found'"
    }
  }

  /*
  {
    data: [
      {
        broadcaster_id: '246753970',
        broadcaster_login: 'dessy_boy',
        broadcaster_name: 'dessy_boy',
        broadcaster_language: 'en',
        game_id: '500561',
        game_name: 'Coding for Carrots',
        title: 'Testing Stuff',
        delay: 0
      }
    ]
  }
  */

  async getUptime (target) {
    const duration = fetch(`https://api.twitch.tv/helix/streams?user_login=${target.substring(1)}`,
    {
      "headers": 
      {
        "Client-ID": this.client_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp => {return this.StreamDuration(target, resp.data[0]);});
    return await duration
  }

  StreamDuration(target, resp){
    if(resp == undefined){return "0"}

    var startDate = moment(resp.started_at);
    var endDate   = moment(new Date());
    var hrsDiff   = (endDate.diff(startDate, 'minutes') / 60).toFixed(0);
    var minsDiff  =  endDate.diff(startDate, 'minutes') % 60;
    var duration  = null;

    if(hrsDiff > 0)
    {
      return hrsDiff + " hours and " + minsDiff + " minutes"
    }
    else
    {
      return minsDiff + " minutes"   
    }
  }

  /*
  {
    data: [
      {
        id: '43252710781',
        user_id: '241257643',
        user_login: 'opah_',
        user_name: 'Opah_',
        game_id: '460630',
        game_name: "Tom Clancy's Rainbow Six Siege",
        type: 'live',
        title: 'If my camera dies one more time i stg ill be angy',
        viewer_count: 8,
        started_at: '2021-08-13T11:59:26Z',
        language: 'en',
        thumbnail_url: 'https://static-cdn.jtvnw.net/previews-ttv/live_user_opah_-{width}x{height}.jpg',
        tag_ids: [Array],
        is_mature: true
      }
    ],
    pagination: {}
  }
  */

  async getFollowage (target, room_id, userid) {
    const duration = fetch(`https://api.twitch.tv/helix/users/follows?to_id=${room_id}&from_id=${userid}`,
    {
      "headers": 
      {
        "Client-ID": cclient_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp => {return this.FollowTime(resp.data[0])});
    return await duration
  }

  FollowTime(resp){
    if(resp == undefined){return "0"}

    var startDate  = moment(resp.followed_at);
    var endDate    = moment(new Date());
    var monthsDiff = endDate.diff(startDate, 'months');

    return monthsDiff
  }

  /*
  {
    total: 1,
    data: [
      {
        from_id: '246753970',
        from_login: 'dessy_boy',
        from_name: 'dessy_boy',
        to_id: '241257643',
        to_login: 'opah_',
        to_name: 'Opah_',
        followed_at: '2021-02-05T23:20:10Z'
      }
    ],
    pagination: {}
  }
  */

  sleep = ms => new Promise(res => setTimeout(res, ms));

  async clip (target, room_id) {
    const clip = fetch(`https://api.twitch.tv/helix/clips?broadcaster_id=${room_id}`,
    {
      "method": 'POST',
      "headers": 
      {
        "Client-ID": this.client_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp =>   {
      return this.getclip(resp, target, room_id)
    });


    return await clip
  }

  async getclip(resp, target, room_id)
  {
    var i = 0
    var clip = null
    while(await clip == null && i < 180)
    {
      clip = this.sleep(500).then(() => {
        const clip_inner = fetch(`https://api.twitch.tv/helix/clips?id=${resp.data[0].id}`,
        {
          "headers": 
          {
            "Client-ID": this.client_id,
            "Authorization": "Bearer " + this.access_token
          }
        })
        .then(resp => resp.json())
        .then(resp => { 
          console.log(resp)
          if(resp.data[0] == undefined){ return null }
          else { return resp.data[0].url }
        })
        return clip_inner
      })
    }
    return await clip
  }

  /*
  { data: [], pagination: {} }

  {
    data: [
      {
        id: 'ImportantEagerTriangleKappa-hY2IQhD76NxUQzIT',
        edit_url: 'https://clips.twitch.tv/ImportantEagerTriangleKappa-hY2IQhD76NxUQzIT/edit'
      }
    ]
  }
  {
    data: [
      {
        id: 'ImportantEagerTriangleKappa-hY2IQhD76NxUQzIT',
        url: 'https://clips.twitch.tv/ImportantEagerTriangleKappa-hY2IQhD76NxUQzIT',
        embed_url: 'https://clips.twitch.tv/embed?clip=ImportantEagerTriangleKappa-hY2IQhD76NxUQzIT',
        broadcaster_id: '246753970',
        broadcaster_name: 'dessy_boy',
        creator_id: '663106856',
        creator_name: 'toxbotto',
        video_id: '',
        game_id: '512354',
        language: 'de',
        title: 'Just bot testing.',
        view_count: 1,
        created_at: '2021-08-13T16:22:38Z',
        thumbnail_url: 'https://clips-media-assets2.twitch.tv/43254297645-offset-4166-preview-480x272.jpg',
        duration: 30
      }
    ],
    pagination: {}
  }
  */  
  async resolveUser (name) {
    const userid = fetch(`https://api.twitch.tv/helix/users?login=${name}`,
    {
      "method": 'GET',
      "headers": 
      {
        "Client-ID": this.client_id,
        "Authorization": "Bearer " + this.access_token
      }
    })
    .then(resp => resp.json())
    .then(resp => { return resp.data[0].id});
    return await userid
  }

  /*


  {
    data: [
      {
        id: '663106856',
        login: 'toxbotto',
        display_name: 'toxbotto',
        type: '',
        broadcaster_type: '',
        description: '',
        profile_image_url: 'https://static-cdn.jtvnw.net/user-default-pictures-uv/de130ab0-def7-11e9-b668-784f43822e80-profile_image-300x300.png',
        offline_image_url: '',
        view_count: 4,
        created_at: '2021-03-16T19:25:34Z'
      }
    ]
  }
  */
}
module.exports.twitchapi = twitchapi;
module.exports.clip = twitchapi.clip;  // -------------------- Del later