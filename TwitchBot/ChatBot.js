const tmi 		= require('tmi.js');
const tmicreds	= require('./../../Credentials/tmi.json');


class tbot {
	tmiclient = null;
	conf       = null;

	constructor(conf)
	{
		conf["identity"] = tmicreds

		this.tmiclient = new tmi.client(conf);
		this.tmiclient.connect();

		this.tmiclient.on('message',      this.messageHandler);
		this.tmiclient.on('connected',    this.connectionHandler);
		this.tmiclient.on('subscription', this.subHandler);
		this.tmiclient.on('subgift',      this.subgiftHandler);
		this.tmiclient.on('resub',        this.resubHandler);
		this.conf = conf;
	}


	async messageHandler (target, context, msg, self) {
		// Return if it was the message of the Bot
		if(self) { return;}
		//Spam detection

		tfunct.spamprotection(target, context, msg) 

		//Pings
		if(configs[target].twitchpings.hasOwnProperty(msg))
		{
			tfunct.send(target,configs[target].twitchpings[msg])
		}

		// Return if msg doesn't have prefix
		if(!msg.startsWith(configs[target].twitchprefix)){return;}
		msg = msg.substring(1);
	
		//Config Commands
		if(configs[target].commands.hasOwnProperty(msg))
		{
			var resopnse = genfunct.Parser(configs[target].commands[msg], configs[target].counters, context, target)
			tfunct.send(target, await resopnse)
		}

		if(msg == "reload")	{genfunct.LoadConfig   (target, true)}
		if(msg == "reset")	{genfunct.ResetCounters(target)}
		if(msg == "help")	{genfunct.Help 		   (target, configs[target].twitchprefix, configs[target].commands)}
	}

	connectionHandler (addr, port) {
		console.log(`* Connected to ${addr}:${port}`);
	}

	subHandler (channel, username, method, message, userstate) {
		var resopnse = genfunct.subparser(configs[target].subcommands["sub"], username, channel, null, null)
		tfunct.send(target, resopnse)
	}

	resubHandler (channel, username, months, message, userstate, methods) {
		let cumulativeMonths = ~~userstate["msg-param-cumulative-months"];
		var resopnse = genfunct.subparser(configs[target].subcommands["resub"], username, channel, null, cumulativeMonths)
		tfunct.send(target, resopnse)
	}	

	subgiftHandler (channel, username, streakMonths, recipient, methods, userstate) {
		tfunct.send(channel,"Thank you " + username + " for gifting a sub to " + recipient + ".");
		
		var resopnse = genfunct.subparser(configs[target].subcommands["subgift"], username, channel, recipient, null)
		tfunct.send(target, resopnse)
	}
}

class twitchfunct
{
	constructor(tbot, cordBot)
	{
		this.tbot      = tbot
		this.cordBot   = cordBot
	}

	send(target, msg)
	{
		this.tbot.tmiclient.say(target,msg)
	}

	spamprotection(target, context, msg) 
	{
		var spamfilter = genfunct.ParseList(configs[target].spamfilter.blacklist)

		for (const element of spamfilter) 
		{
			if(msg.includes(element) && context.mod == false)
			{
				//Log to discord	        
				msg = "Username:\n" + context.username + "\n\nMessage:\n" + msg + "\n\nStream:\nhttps://www.twitch.tv/" + target.substring(1) + "/" + "\n\nUserId:\n" + context['user-id'] + "\n\nMessageId:\n" + context.id
				cordBot.spamreport(target, msg)

				//Delete message and Response
				var resopnse = genfunct.ParseListGetRandom(configs[target].spamfilter.responses)
				this.tbot.tmiclient.deletemessage(target, context.id)
				this.tbot.tmiclient.say          (target, resopnse)
				return
			}
		}
	}

}

module.exports.tbot = tbot;
module.exports.twitchfunct = twitchfunct;