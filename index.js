const conf	    = require('./Confs/Conf.json');
const twitch    = require('./TwitchBot/ChatBot.js');
const twitchapi = require('./TwitchBot/API.js');
const discord   = require('./DiscordBot/DBotto.js');
const generalfunctions = require('./GeneralFunctions/GeneralFunctions.js');

global.credpath		= "./../Credentials/";
global.configs   	= {}
global.timedmsgs 	= {}

cordBot   = new discord.dbot(conf.discord);
tchatBot  = new twitch.tbot(conf.tmi);


global.tfunct    	= new twitch.twitchfunct(tchatBot, cordBot);
global.genfunct  	= new generalfunctions.generalfunctions(conf.channels);
global.tapi      	= new twitchapi.twitchapi(conf.channels);


