# Project
The project contained in this Repository is a Twtich/Discordbot that uses a parser to create custom commands for your twitch chat.

# Parser Variables Syntax

## Basic commands:
Basic Commands can contain normal Variables, Counters, Lists and Simple If clauses. All the three operations can be used multiple times and combined in a single message.

Syntax:

    {
        <command>:<response message with variables, counters, lists and if clauses>
    }

### Variables

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| {user}                        | Inserts the username of the user which issued the command into the response message.                  |
| {broadcaster}                 | Inserts the username of the broadcaster into the response message.                                    |
| {followage}                   | Inserts the following time of the user which issued the command into the response message.            |
| {sub}                         | Inserts the subscription time of the user which issued the command into the response message.         |
| {title}                       | Inserts the stream title into the response message.                                                   |
| {game}                        | Inserts the game name into the response message.                                                      |
| {uptime}                      | Inserts the stream uptime into the response message.                                                  |
| {clip}                        | Automatically Creates a 30 second clip and inserts the clips url into the response message.           |

### Counters

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
|{ctr:countername:inc}          | Increments the counter countername                                                                    |
|{ctr:countername:show}         | Inserts the current number of the counter countername into the response message.                      |

### Lists

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| [opt1, opt2, ... optn]        | The list variable inserts one of the possible options into the response message.                      |


### If Clauses

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
|(Var1==Var2:Op1:Opt2)          | The If Cluase inserts Opt1 if Var1 equals Var2 or insterts Opt2 if they are not equal.                |
|(Var1!=Var2:Op1:Opt2)          | The If Cluase inserts Opt1 if Var1 is not equal to Var2 or insterts Opt2 if they are equal.           |

### Time Based Messages:

Time based messages are sent periodically into the chat for advertising purposes and allow the usage of some simple variables.

Syntax of a time based message:

    {"minutes": 30, "msg":<message with variables>}

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| {broadcaster}                 | Inserts the username of the broadcaster into the response message.                                    |
| {title}                       | Inserts the stream title into the response message.                                                   |
| {game}                        | Inserts the game name into the response message.                                                      |
| {uptime}                      | Inserts the stream uptime into the response message.                                                  |

### Subcription Messages:

This type of messages are tirggered by Subs, Resubs or Subgifts.

Syntax:

    {
        "sub"             : <message with variables>,
        "resub"           : <message with variables>,
        "subgift"         : <message with variables>
    }

#### Sub Varaibles

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| {user}                        | Inserts the username of the user which subscribed.                                                    |
| {channel}                     | Inserts the channel name into the response message.                                                   |

#### Resub Varaibles

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| {user}                        | Inserts the username of the user which subscribed.                                                    |
| {channel}                     | Inserts the channel name into the response message.                                                   |
| {months}                      | Inserts the number of months the user has subscribed.                                                 |

#### Resub Varaibles

| Variable                      | Output                                                                                                |
|-------------------------------|-------------------------------------------------------------------------------------------------------|
| {user}                        | Inserts the username of the user which gifted the sub.                                                |
| {recipient}                   | Inserts the username of the person which recieved the subscription into the response.                 |
