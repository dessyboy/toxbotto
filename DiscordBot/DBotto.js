const Discord 		= require('discord.js');
const DiscordCreds	= require('./../../Credentials/discord.json');

class dbot {
	cordclient = null;
	conf       = null;

	constructor(conf)
	{
		this.cordclient = new Discord.Client();
		this.cordclient.login(DiscordCreds.Token);
		this.cordclient.on('ready', this.connectionHandler);
		this.conf = conf;
	}

	spamreport(target, msg)
	{
		if(configs[target].hasOwnProperty("discord"))
		{
			if(configs[target].discord.hasOwnProperty("logchannel"))
			{
				let channelid = configs[target].discord.logchannel
				let channel   = this.cordclient.channels.cache.get(channelid)

				//Change to embed
				channel.send("```" + msg + "```")
			}
		}
	}

	connectionHandler () {
  		console.log(`Connected to Discord`);
  		
  		tfunct.cordBot.cordclient.user.setPresence({ activity: { name: 'this server. You can support my creator on www.patreon.com/dessyboy_coding if you enjoy my service.', type: 'WATCHING' }, status: 'dnd' })
  		.catch(console.error);

	}
}
module.exports.dbot       = dbot;
